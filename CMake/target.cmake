set(EXECUTABLE_SOURCE_LIST)

set(EXECUTABLE_INCLUDE_LIST
  ${BIN_SRC_DIR}/sys/ProjectConfig.hpp #the CMake generated project config file
  )


#SYSTEM
set(SYS_SOURCES)
set(SYS_INCLUDES)

if(UNIX)

#POSIX SYSTEM

set(SYS_POSIX_SOURCE_LIST
  ${SYSD}/posix/sys.cxx
  ${SYSD}/posix/main.cxx
  )

#file(GLOB SYS_POSIX_SOURCE_LIST ${SYSD}/posix/*.cxx)
#source_group("sys\\posix" FILES ${SYS_POSIX_SOURCE_LIST})

list(APPEND SYS_SOURCES
    ${SYS_POSIX_SOURCE_LIST}
    )

endif()

#SYSTEM LIBRARY

set(SYS_INCLUDE_LIST
  ${SYSD}/sys_defines.hpp
  ${SYSD}/sys_includes.hpp
  ${SYSD}/sys_public.hpp
  )
#file(GLOB SYS_INCLUDE_LIST ${SYSD}/*.h)
#source_group("sys" FILES ${SYS_INCLUDE_LIST})

list(APPEND SYS_INCLUDES
    ${SYS_INCLUDE_LIST}
    )

#add_library(sys_lib ${SYS_SOURCES} ${SYS_INCLUDES})

list(APPEND EXECUTABLE_SOURCE_LIST
    ${SYS_SOURCES}
    )

list(APPEND EXECUTABLE_INCLUDE_LIST
    ${SYS_INCLUDES}
    )

#MAIN EXECUTABLE
add_executable(primer
  ${EXECUTABLE_SOURCE_LIST}
  ${EXECUTABLE_INCLUDE_LIST}
  )
target_include_directories(primer
                           PRIVATE ${BIN_SRC_DIR}/sys/
                           )
set_target_properties(primer PROPERTIES
			RUNTIME_OUTPUT_DIRECTORY ${BIN_RELEASE_DIR}
			)

message(VERBOSE, "main targets done")

#accompaniying libraries

#example:
# Build the GL3 dynamic library
#add_library(ref_gl3 MODULE ${GL3-Source} ${GL3-Header} ${REF-Platform-Specific-Source})
#set_target_properties(ref_gl3 PROPERTIES
#		PREFIX ""
#		LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/release
#		RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/release
#		SUFFIX ${CMAKE_SHARED_LIBRARY_SUFFIX}
#		)
#target_link_libraries(ref_gl3 ${yquake2LinkerFlags} ${yquake2SDLLinkerFlags})
