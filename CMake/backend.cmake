#
#this program is GPLv3
#

set(BACKEND_INCLUDES)
set(BACKEND_SOURCES)

#FRAMEWORK
set(FRAMEWORK_INCLUDES
  ${BED}/framework/session.hpp
  ${BED}/framework/sessionLocal.hpp
  ${BED}/framework/shell.hpp
  ${BED}/framework/shellLocal.hpp
  ${BED}/framework/common.hpp
  ${BED}/framework/commonLocal.hpp

  )
set(FRAMEWORK_SOURCES
  ${BED}/framework/common.cxx
  ${BED}/framework/session.cxx
  ${BED}/framework/shell.cxx
  )

#LIB MATHS
set(LIB_MATHS_INCLUDES
	${BED}/lib/maths/maths.hpp
	${BED}/lib/maths/vectors.hpp
	)

set(LIB_MATHS_SOURCES
	${BED}/lib/maths/maths.cxx
	${BED}/lib/maths/vectors.cxx
	${BED}/lib/maths/xdVectors.cxx
	)

#OPENGL
set(BE_RENDER_OGL_INCLUDES
  ${BED}/render/OpenGL/OGL_base.hpp
  )
set(BE_RENDER_OGL_SOURCES
	${BED}/render/OpenGL/OGL_base.cxx
	)

#GLSL
set(BE_RENDER_GLSL_INCLUDES
	${BED}/render/GLSL/shaders.hpp
	)
set(BE_RENDER_GLSL_SOURCES
	${BED}/render/GLSL/shaders.cxx
	)

#file(GLOB BE_RENDER_OGL_INCLUDES ${BED}/render/OpenGL/*.hpp)
#source_group("render\\OpenGL" FILES ${BE_RENDER_OGL_INCLUDES})

#SDL
set(BE_SDL_INCLUDES
  ${BED}/sdl/SDL_base.hpp
  )

set(BE_SDL_SOURCES
  ${BED}/sdl/SDL_base.cxx
  )
#file(GLOB BE_SDL_INCLUDES ${BED}/sdl/*.hpp)
#source_group("sdl" FILES ${BE_SDL_INCLUDES})

#file(GLOB BE_SDL_SOURCES ${BED}/sdl/*.cxx)
#source_group("sdl" FILES ${BE_SDL_SOURCES})

#RENDER SYSTEM
set(BE_RENDER_INCLUDES
  ${BED}/render/renderSystem.hpp
  ${BED}/render/renderSystemLocal.hpp
  ${BED}/render/meshes.hpp
  )

set(BE_RENDER_SOURCES
  ${BED}/render/renderSystem.cxx
  ${BED}/render/meshes.cxx
  )

#file(GLOB BE_RENDER_INCLUDES ${BED}/render/*.hpp)
#source_group("render" FILES ${BE_RENDER_INCLUDES})

#file(GLOB BE_RENDER_SOURCES ${SYSD}/render/*.cxx)
#source_group("render" FILES ${BE_RENDER_SOURCES})

list(APPEND BACKEND_INCLUDES
  ${FRAMEWORK_INCLUDES}
  ${LIB_MATHS_INCLUDES}
  ${BE_RENDER_OGL_INCLUDES}
  ${BE_RENDER_GLSL_INCLUDES}
  ${BE_SDL_INCLUDES}
  ${BE_RENDER_INCLUDES}
  )
list(APPEND BACKEND_SOURCES
  ${FRAMEWORK_SOURCES}
  ${LIB_MATHS_SOURCES}
  ${BE_RENDER_OGL_SOURCES}
  ${BE_RENDER_GLSL_SOURCES}
  ${BE_SDL_SOURCES}
  ${BE_RENDER_SOURCES}
  )

add_library(backend ${BACKEND_SOURCES} ${BACKEND_INCLUDES})
