#CMAKE MODULES
#set(STANDARD_CURRENT_LIST_DIR ${CMAKE_CURRENT_LIST_DIR})
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_DIR}/Modules/")

#OPENGL
find_package(OpenGL REQUIRED)
if (OpenGL_OpenGL_FOUND)
    include_directories(${OPENGL_INCLUDE_DIRS})
    target_link_libraries(backend ${OPENGL_LIBRARIES})
endif()

#GLEW
find_package(GLEW REQUIRED)
if (GLEW_FOUND)
    include_directories(${GLEW_INCLUDE_DIRS})
    target_link_libraries(backend ${GLEW_LIBRARIES})
    #TODO maybe we should create first the targets and then target_link_libraries() to them latter?
endif()

#SDL2
find_package(SDL2 REQUIRED)
if( SDL2_FOUND )
  include_directories(${SDL2_INCLUDE_DIR})
  target_link_libraries(backend ${SDL2_LIBRARY})
endif()


