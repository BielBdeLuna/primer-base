#
#this program is GPLv3
#

project(Primer VERSION 0.1.4)

#options########################################################################

#option(MONOLITH
#  "Embed interactive logic into main executable" ON)

####
#source dirs
set(CMAKE_DIR ${CMAKE_CURRENT_LIST_DIR})
set(SRC_DIR ${CMAKE_DIR}/../src)
set(BED ${SRC_DIR}/backend)
set(FED ${SRC_DIR}/frontend)
set(SYSD ${SRC_DIR}/sys)

#binary dirs
set(BIN_SRC_DIR ${CMAKE_BINARY_DIR}/src)
set(BIN_RELEASE_DIR ${CMAKE_BINARY_DIR}/release)

# configure a header file to at the moment only pass the version number
configure_file(ProjectConfig.h.in ${BIN_SRC_DIR}/sys/ProjectConfig.hpp)
message(VERBOSE, "'ProjectConfig.h' created")

## Compiler specific options
if(CMAKE_COMPILER_IS_GNUCC OR CMAKE_C_COMPILER_ID STREQUAL "Clang")
  # GCC includes MinGW
  include(${CMAKE_DIR}/gcc_defs.cmake)
elseif(MSVC)
  include(${CMAKE_DIR}/msvc.cmake)
endif() # MSVC


include(${CMAKE_DIR}/target.cmake)

## backend
include(${CMAKE_DIR}/backend.cmake)

## frontend
include(${CMAKE_DIR}/frontend.cmake)

message(VERBOSE, "program code done")

## EXTERNAL LIBRARIES
include(${CMAKE_DIR}/external_libs.cmake)

## TARGETS
#if(UNIX) # Linux, OSX, *BSD, Solaris, ...
#  include(${SRC_DIR}/target_unix.cmake)
#elseif(WIN32) # Windows (MSVC and MinGW, 32 and 64bit)
#  include(${SRC_DIR}/windows.cmake)
#else()
  # if your platform is neither windows nor unix-ish, you'll have to adapt the
  # cmake files for it (and you'll probably also have to write some system-specific code)
#  message(FATAL_ERROR, "Unsupported platform!")
#endif()

#LINKING
include(${CMAKE_DIR}/linking.cmake)

include(${CMAKE_DIR}/copy_files.cmake)

#### install and packaging
