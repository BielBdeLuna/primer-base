/*
this program is GPLv3
*/

#ifndef __SYS_INCLUDES_H__
#define __SYS_INCLUDES_H__

#include <cstdio>
#include <cstdarg>
#include <cstring>

//#include <stdio.h>
#include <iostream>
#include <string>
//#include <stdarg.h>

//posix stuff
//#if defined (__unix__) || (defined (__APPLE__) && defined (__MACH__))

#if defined(POSIX_COMPAT)
#include <unistd.h> //posix standard lib
#endif



#endif /* !__SYS_INCLUDES_H__ */
