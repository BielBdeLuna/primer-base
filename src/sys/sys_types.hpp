/*
this program is GPLv3
*/

#ifndef __SYS_TYPES_H__
#define __SYS_TYPES_H__

//#include "../sys/sys_assert.h"
#include "sys_includes.hpp"

namespace primer
{

typedef unsigned char		    byte;
typedef unsigned short      word;
typedef unsigned int        dword;
typedef unsigned int        uint;

typedef signed char         int8;
typedef unsigned char       uint8;
typedef short int           int16;
typedef unsigned short int  uint16;
typedef int                 int32;
typedef unsigned int        uint32;
typedef long long           int64;
typedef unsigned long long  uint64;
/*
assert_sizeof( bool,	1 );
assert_sizeof( char,	1 );
assert_sizeof( short,	2 );
assert_sizeof( int,		4 );
assert_sizeof( float,	4 );
assert_sizeof( byte,	1 );
assert_sizeof( int8,	1 );
assert_sizeof( uint8,	1 );
assert_sizeof( int16,	2 );
assert_sizeof( uint16,	2 );
assert_sizeof( int32,	4 );
assert_sizeof( uint32,	4 );
assert_sizeof( int64,	8 );
assert_sizeof( uint64,	8 );
*/

} // namespace primer
#endif /* !__SYS_TYPES_H__ */
