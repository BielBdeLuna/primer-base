/*
this program is GPLv3
*/


#include "../sys_defines.hpp"
#include "../sys_public.hpp"

#include "../../backend/framework/common.hpp"
#include <ProjectConfig.hpp>

int main(int argc, char* argv[])
{
  /* Prevent running as root.*/
	if (getuid() == 0)
	{
		Sys_Error( "this shouldn't be run as root!" );
	}

	/* Enforce the real UID to prevent problems. */
	if (getuid() != geteuid())
	{
		Sys_Error( "The effective UID is not the real UID! Your binary is probably marked 'setuid'." );
	}

  /* proper usage. */

  int FPSinputValue = 0;

  Sys_Printf( "Biel's primer %s %i.%i.%i\n", BUILD_STRING, PRIMER_VERSION_MAJOR, PRIMER_VERSION_MINOR, PRIMER_VERSION_REVISION );

  if (argc < 2) {
    Sys_Printf( "Usage: %s and a number, that will serve as the fps cap( 60 for 60fps )\n", argv[0] );
    Sys_Printf( "and don't use it with sudo goddammit!\n" );
    //Sys_Quit();
    Sys_Printf( "assuming 60fps then...\n" );
    FPSinputValue = 60;
  } else {
    FPSinputValue = std::stoi(argv[1]);

    if( FPSinputValue >= 200 ) {
      Sys_Error( "GET OUT OF HERE! too many FPS!\n" );
    } else if ( FPSinputValue < 10 ) {
      if( FPSinputValue != 0 ) {
        Sys_Error( "GET OUT OF HERE! too little ambition!\n" );
      }
    }
  }

  Sys_Milliseconds();

  primer::common->Init( FPSinputValue );

  Sys_Printf( "Starting interaction now...\n\n" );

  primer::common->Run();
}
