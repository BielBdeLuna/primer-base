/*
this program is GPLv3
*/

#include "../sys_public.hpp"
#include <time.h>
#include <chrono>
#include <thread>

//TODO for the filesystem
#include <fstream>
#include <sstream>

void Posix_Exit( int ret ) {
  std::exit( ret );
}

void Sys_Quit() {
  Posix_Exit( EXIT_SUCCESS );
}

void Sys_ErrorOut() {
  Posix_Exit( EXIT_FAILURE );
}

unsigned int sys_timeBase = 0;

int Sys_Milliseconds() {
  int curtime;
	struct timespec ts;

	clock_gettime( CLOCK_MONOTONIC, &ts );

	if( !sys_timeBase )
	{
		sys_timeBase = ts.tv_sec;
		return ts.tv_nsec / 1000000;
	}

	curtime = ( ts.tv_sec - sys_timeBase ) * 1000 + ts.tv_nsec / 1000000;

	return curtime;

}

void Sys_Printf( const char* fmt, ... ) {
  va_list args;
  va_start( args, fmt );
  vprintf( fmt, args );
  va_end( args );

}

void Sys_Error( const char * fmt, ... ) {
  va_list args;

  Sys_Printf( "Sys_Error: " );

  va_start( args, fmt );
  vprintf( fmt, args );
  va_end( args );

  Sys_Printf( "\n" );

  Sys_ErrorOut();
}

void Sys_SleepMS( int time ) {
  std::this_thread::sleep_for( std::chrono::milliseconds( time ) );
}

//TODO for the filesystem
std::string Sys_ReadFile( const char* file ) {
  // Open file
  std::ifstream t(file);

  // Read file into buffer
  std::stringstream buffer;
  buffer << t.rdbuf();

  // Make a std::string and fill it with the contents of buffer
  std::string fileContent = buffer.str();

  return fileContent;
}
