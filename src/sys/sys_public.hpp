/*
this program is GPLv3
*/

#include "sys_includes.hpp"

void Posix_Exit( int ret );
void Sys_Quit();
void Sys_ErrorOut();

int Sys_Milliseconds();

void Sys_Printf( const char* s, ... );
void Sys_Error( const char* s, ... );

void Sys_SleepMS( int time );

//TODO we should have a filesystem
std::string Sys_ReadFile( const char* file );


