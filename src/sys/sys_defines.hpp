/*
this program is GPLv3
*/

#ifndef __SYS_DEFINES_H__
#define __SYS_DEFINES_H__

/*
 * system wide defines
 */

// POSIX
#if defined(__linux__) || defined(__FreeBSD__) || defined(__APPLE__)

#define POSIX_COMPAT

#if defined(__i386__)
#define	ISA						  "x86"
#elif defined(__x86_64__)
#define ISA					  "x86_64"
#endif //ISA

#if defined(__FreeBSD__)
#define	BUILD_STRING					"freebsd-" ISA
#elif defined(__linux__)
#define	BUILD_STRING					"linux-" ISA
#elif defined(__APPLE__)
#define BUILD_STRING					"osx-" ISA
#endif //BUILD_STRING

#endif // POSIX

#endif /* !__SYS_DEFINES_H__ */
