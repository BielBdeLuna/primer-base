/*
this program is GPLv3
*/

#ifndef __SHELLLOCAL_HPP__
#define __SHELLLOCAL_HPP__

#include "shell.hpp"
#include "../../frontend/content/content.hpp"

namespace primer {

class blShellLocal : public blShell {
public:
  blShellLocal();

  bool Init() override;
  void Shutdown() override;

  void Loading() override;
  void Interactive() override;
  void Finish() override;

private:
  blContent content;

  bool IsTrue;
  int nextTime;
};

extern blShellLocal shellLocal;

} //namespace primer
#endif /* !_SHELLLOCAL_HPP__ */
