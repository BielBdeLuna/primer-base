/*
this program is GPLv3
*/


#include "common.hpp"
//#include <sys/time.h>

#include "../../frontend/content/content.hpp"

namespace primer {

#define	PRINT_MSG_MAX_SIZE	4096

inline float MStoFPS( int timeMS ) { return 1 / ( (float)timeMS / 1000 ); };
inline int FPStoMS( int fps ) { return 1000 / fps; };

/*
struct timespec {
        time_t   tv_sec;        // seconds
        long     tv_nsec;       // nanoseconds
};
*/

struct stepTimes_t
{
  int desiredFrameTimeMS;
	int	startStepTime;
	int	finishStepTime;
  int deltaStepTime() { return finishStepTime - startStepTime; };
};

class blCommonLocal : public blCommon {

public:
  blCommonLocal();

  void Init( int fps_value ) override;
  void Shutdown() override;
  void Terminate() override;
  void Printf( const char* formated, ... ) override;
  void Warning( const char* formated, ... ) override;
  void Error( const char* formated, ... ) override;
  void ErrorOut( const char* text ) override;

  void Run() override;
private:

  stepTimes_t StepTimes;

  void Clear();

  blContent content;
};

extern blCommonLocal commonLocal;

} // namespace primer
