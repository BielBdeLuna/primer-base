/*
this program is GPLv3
*/

#ifndef __SESSIONLOCAL_H__
#define __SESSIONLOCAL_H__

#include "session.hpp"
#include "../../frontend/content/content.hpp"

namespace primer {

class blSessionLocal : public blSession {
public:
  blSessionLocal();
  bool Init() override;
  void Shutdown() override;

  void Start() override;
  void Stop() override;

  session_e GetStatus() override;

};

extern blSessionLocal sessionLocal;

} // namespace primer
#endif /* !__SESSIONLOCAL_H__ */
