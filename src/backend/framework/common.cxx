/*
this program is GPLv3
*/

#include "commonLocal.hpp"
#include "../render/renderSystem.hpp"
#include "session.hpp"
#include "shell.hpp"

namespace primer {

blCommonLocal	commonLocal;
blCommon* 		common = &commonLocal;

blCommonLocal::blCommonLocal() {
  Clear();
  StepTimes.finishStepTime = Sys_Milliseconds();
}

void blCommonLocal::Clear() {
  return;
}

void blCommonLocal::Printf( const char* formated, ... ) {
  //std::cout << s << std::endl;
  char msg[PRINT_MSG_MAX_SIZE];
  //msg[ 0 ] = '\0';
  va_list args;
  va_start( args, formated );
  //ReplacePrintArgs( msg, PRINT_MSG_MAX_SIZE, formated, args );
  std::vsnprintf( msg, PRINT_MSG_MAX_SIZE, formated, args );
  //std::cout << msg << std::endl;
  Sys_Printf( msg );
  va_end( args );
}

void blCommonLocal::Warning( const char* formated, ... ) {
  Printf( "WARNING: " );
  char msg[PRINT_MSG_MAX_SIZE];
  va_list args;
  va_start( args, formated );
  std::vsnprintf( msg, PRINT_MSG_MAX_SIZE, formated, args );
  Sys_Printf( msg );
  va_end( args );
}

void blCommonLocal::Error( const char* formated, ... ) {
  Printf( "ERROR: " );
  char msg[PRINT_MSG_MAX_SIZE];
  va_list args;
  va_start( args, formated );
  std::vsnprintf( msg, PRINT_MSG_MAX_SIZE, formated, args );
  va_end( args );
  //Sys_Error( msg );
  ErrorOut( msg );
}

void blCommonLocal::ErrorOut( const char* text ) {
  //at least let's try to get out gracefully
  Shutdown();
  Sys_Error( text );
}

void blCommonLocal::Init( int fps_value ) {
  Printf( "----> Common init <----\n" );

  if( fps_value ) {
    StepTimes.desiredFrameTimeMS = FPStoMS( fps_value );
    Printf( "fps_value is %i and desiredFrameTimeMS is %i.\n", fps_value, StepTimes.desiredFrameTimeMS );
  } else {
  StepTimes.desiredFrameTimeMS = 0;
    Printf( "frame unlocked!\n" );
  }

   if( !session->Init() ) {
    common->Error( "blCommonLocal::Init: couldn't init the Session!\n");
  }

  if( !shell->Init() ){
    common->Error( "blCommonLocal::Init: couldn't init the Shell!\n");
  }

  if( !renderSystem->Init() ) {
    common->Error( "blCommonLocal::Init: couldn't init the Render System!\n");
  }

  Printf( " -- Common initialized --\n" );
}

void blCommonLocal::Shutdown() {
  Printf( "----> Common shutting down <----\n" );

  session->Shutdown();

  shell->Shutdown();

  renderSystem->Shutdown();

  Printf( "----> Common shut down <----\n" );
}

void blCommonLocal::Terminate() {
  Shutdown(); //TODO only if we aren't failing
  Sys_Quit();
}

void blCommonLocal::Run() {
  while( session->GetStatus() != SHUTDOWN ) {
    //TODO change to a more sophisticated way to unlock framerate

    //gather timmings
    int curTime = Sys_Milliseconds();
    StepTimes.startStepTime = curTime;

    //draw the step
    //Sys_SleepMS( 20 ); //DEBUG let's make a load far more time heavy than what is possible at less than 48 fps
    //if ( session->GetStatus() == INIT ) {
    //  shell->Loading();
    //} else {
      shell->Interactive();
    //}

    //if we have a desired frame rate then try to maintain it in case we finish earlier than the wanted fps
    if( StepTimes.desiredFrameTimeMS ) { //not unlocked framerate
      int endTime = StepTimes.startStepTime + StepTimes.desiredFrameTimeMS;
      curTime = Sys_Milliseconds();
      if( curTime < endTime ) {
        //stall until we meet the correct time
        Sys_SleepMS( endTime - curTime );
      }
    }
    StepTimes.finishStepTime = Sys_Milliseconds();
    int delta = StepTimes.deltaStepTime();
    //Printf( "step time delta is %i this is %4.2f FPS.\n", delta, MStoFPS( delta ) );
  }
  //shell->Finish();
  Terminate();
}

} // namespace primer
