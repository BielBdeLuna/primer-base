/*
this program is GPLv3
*/

#include "../../sys/sys_public.hpp"
#include "shellLocal.hpp"
#include "session.hpp"
#include "common.hpp"
#include "../sdl/SDL_base.hpp"
#include "../render/renderSystem.hpp"

namespace primer {

blShellLocal	shellLocal;
blShell* 		shell = &shellLocal;

blShellLocal::blShellLocal() {
  nextTime = 0;
  IsTrue = false;
}

bool blShellLocal::Init() {
  common->Printf( " -- Shell init --\n" );
  content.Init();
  renderSystem->SetContent( content );
  common->Printf( " -- Shell initialized --\n" );
  return true;
}

void blShellLocal::Shutdown() {
  common->Printf( " -- Shell shut down --\n" );
}

void blShellLocal::Loading() {
  ;
}

void blShellLocal::Interactive() {
  int curTime = Sys_Milliseconds();
  SDL_Event event;
	while( SDL_PollEvent( &event ) ) {
	  if( event.type == SDL_QUIT ) {
	    common->Printf( "Window closed\n" );
		  session->Stop();
    }

		if( event.type == SDL_KEYDOWN ) {
		  switch( event.key.keysym.sym ) {
			case SDLK_ESCAPE:
			  common->Printf( "ESC pressed\n" );
			  session->Stop();
				break;
			case SDLK_SPACE:
			  if( curTime >= nextTime ) {
			    common->Printf( "SPACE pressed\n" );
			    if( IsTrue ) {
			      renderSystem->Renderer_Render_B();
			    } else {
			      renderSystem->Renderer_Render_A();
			    }
			    IsTrue = !IsTrue;
			    nextTime = curTime + 10;
			    renderSystem->SwapWindow();
			  }
			  break;
			default:
				break;
			}
		}
	}
}

void blShellLocal::Finish() {
  ;
}

} //namespace primer
