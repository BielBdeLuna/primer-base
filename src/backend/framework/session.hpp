/*
this program is GPLv3
*/

#ifndef __SESSION_H__
#define __SESSION_H__

namespace primer {

enum session_e {
  NONE = -1,
  INIT,
  START_INTERACTION,
  INTERACTION,
  INTERRUP_INTERACTION,
  SHUTDOWN
};

class blSession {
public:
  virtual ~blSession() {};
  virtual bool Init() = 0;
  virtual void Shutdown() = 0; // what to kill when stopping even not gracefully

  virtual void Start() = 0; //start interaction
  virtual void Stop() = 0; //stop gracefull

  virtual session_e GetStatus() = 0;
protected:
  session_e sessStatus;
};

extern blSession* session;

} // namespace primer
#endif /* !__SESSION_H__ */
