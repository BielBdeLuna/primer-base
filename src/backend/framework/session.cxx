/*
this program is GPLv3
*/

#include "sessionLocal.hpp"
#include "common.hpp"

namespace primer {

blSessionLocal  sessionLocal;
blSession*      session = &sessionLocal;

blSessionLocal::blSessionLocal() {
  sessStatus = NONE;
}

bool blSessionLocal::Init() {
  common->Printf( " -- Session init --\n" );
  sessStatus = INIT;
  common->Printf( " -- Session initialized --\n" );
  return true;
}

void blSessionLocal::Shutdown() {
  sessStatus = SHUTDOWN;
  common->Printf( " -- Session shut down --\n" );
}

void blSessionLocal::Start() {
  sessStatus = START_INTERACTION;
}

void blSessionLocal::Stop() {
  sessStatus = INTERRUP_INTERACTION;
}

session_e blSessionLocal::GetStatus() {
  switch( sessStatus ) {
    case INIT:
      //we forgot to start?!
      Start();
      break;
    case START_INTERACTION:
      //next frame will be normal
      sessStatus = INTERACTION;
      break;
    case INTERRUP_INTERACTION:
      //wrap arround in the next frame as we will be shutting down!
      sessStatus = SHUTDOWN;
    default:
      break;
  };
  return sessStatus;
}


} // namespace primer
