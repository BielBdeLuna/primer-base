/*
this program is GPLv3
*/

#ifndef __STR_H__
#define __STR_H__

#include "../../sys/sys_includes.h"

namespace primer {

int const STR_ALLOCATED_SIZE = ( sizeof( char* ) == 4 ) ? 20 : 32;

class blStr {
public:
  blStr();
  blStr( const blStr& text );
  blStr( const char* text );

  const char*     toChar() const;
protected:
  char	  buffer[ STR_ALLOCATED_SIZE ];
  char*   data;
  int     lenght;
}

} // namespace primer
#endif /* !__STR_H__ */
