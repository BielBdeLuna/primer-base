/*
this program is GPLv3
*/

#ifndef __SHELL_HPP__
#define __SHELL_HPP__

#include "../../sys/sys_includes.hpp"

namespace primer {

class blShell {
public:
  virtual ~blShell() {}

  virtual bool Init() = 0;
  virtual void Shutdown() = 0;

  virtual void Loading() = 0;
  virtual void Interactive() = 0;
  virtual void Finish() = 0;
};

extern blShell*	shell;

} //namespace primer
#endif /* !__SHELL_HPP__ */
