/*
this program is GPLv3
*/

#ifndef __COMMON_H__
#define __COMMON_H__

#include "../../sys/sys_public.hpp"

namespace primer {

class blCommon {

public:
  virtual ~blCommon() {}

  virtual void Init( int fps_value ) = 0;
  virtual void Shutdown() = 0;
  virtual void Terminate() = 0;

  virtual void Printf(const char *formated, ... ) = 0;
  virtual void Warning(const char *formated, ... ) = 0;
  virtual void Error(const char *formated, ... ) = 0;
  virtual void ErrorOut( const char* text ) = 0;

  virtual void Run() = 0;
};

extern blCommon* 		common;

} // namespace primer

#endif /* !__COMMON_H__ */
