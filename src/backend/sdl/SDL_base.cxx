/*
this program is GPLv3
*/

#include "../framework/common.hpp"
#include "SDL_base.hpp"

namespace primer {

void GLEW_init() {
  glewExperimental = GL_TRUE;
	glewInit();
}

void SDL_CheckError( int line = -1 ) {
	std::string error = SDL_GetError();
	if (error != "")
	{
	  Sys_Printf( "SDL Error : %s\n", error.c_str() );
	  if( line != -1 ) {
	    Sys_Printf( "\nLine : %i", line );
	  }
	  SDL_ClearError();
	}
}

void SDL_InitVideo( SDLwindow_t &window ) {
  // Initialize SDL's Video subsystem
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
	  common->Error( "Failed to init SDL\n" );
	}

	window.window = SDL_CreateWindow( window.title,
	  window.pos_x, window.pos_y,	window.size_x, window.size_y, SDL_WINDOW_OPENGL);

	// Check that everything worked out okay
	if ( !window.window )
	{
	  common->Printf( "Unable to create window\n" );
		SDL_CheckError(__LINE__);
		common->ErrorOut( "" );
	}

}

void SDL_CreateContext( SDLwindow_t &window, SDL_GLContext context ) {
  // Create our opengl context and attach it to our window
	context = SDL_GL_CreateContext( window.window );
}

bool SDL_SetOpenGLatributes( OpenGL_config_t &OGLatributes ) {
	// Set our OpenGL version.
	// SDL_GL_CONTEXT_CORE gives us only the newer version, deprecated functions are disabled
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, OGLatributes.mask_profile );

	// 3.2 is part of the modern versions of OpenGL, but most video cards whould be able to run it
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, OGLatributes.version_major );
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, OGLatributes.version_minor );

	// Turn on double buffering with a 24bit Z buffer.
	// You may need to change this to 16 or 32 for your system
	SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, OGLatributes.doubleBuffered );

	return true;
}

void SDL_ClearBuffer( SDLwindow_t &window, Vec4d colour ) {
  // Clear our buffer with a coloured background
  glClearColor( colour.GetData( 0 ), colour.GetData( 1 ), colour.GetData( 2 ), colour.GetData( 3 ) );
	glClear(GL_COLOR_BUFFER_BIT);
	SDL_GL_SwapWindow( window.window );
}

void SDL_SwapWindow( SDLwindow_t &window ) {
  SDL_GL_SwapWindow( window.window );
}

void SDL_Shutdown( SDLwindow_t &window, SDL_GLContext context ) {

  // Delete our OpengL context
	SDL_GL_DeleteContext( context );

	// Destroy our window
	SDL_DestroyWindow( window.window );

  // Shutdown SDL2
	SDL_Quit();
}

void SDL_PrintOpenGLatributes() {
  int maj, min, cont;
  SDL_GL_GetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, &maj);
  SDL_GL_GetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, &min);
  SDL_GL_GetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, &cont);

  if( cont == SDL_GL_CONTEXT_PROFILE_CORE ) {
    common->Printf( "SDL: Running a SDL window with a OpenGL %i.%i in a core context.\n", maj, min );
  } else {
    common->Printf( "SDL_GL_CONTEXT_MAJOR_VERSION : %i\n", maj );
  	common->Printf( "SDL_GL_CONTEXT_MINOR_VERSION : %i\n", min );
  }
}

} // namespace primer
