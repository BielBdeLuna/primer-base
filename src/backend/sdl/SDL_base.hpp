/*
this program is GPLv3
*/

#ifndef __SIMPLEDIRECTMEDIALAYER_H__
#define __SIMPLEDIRECTMEDIALAYER_H__

#include "../render/OpenGL/OGL_base.hpp"
#include <SDL2/SDL.h>

#include "../lib/maths/vectors.hpp"

namespace primer {

struct SDLwindow_t {
  SDL_Window* window;
  int size_x;
  int size_y;
  int pos_x;
  int pos_y;
  const char* title;
};

void SDL_CheckError( int line );

void GLEW_init();

bool SDL_SetOpenGLatributes( OpenGL_config_t &OGLatributes );

void SDL_ClearBuffer( SDLwindow_t &window, Vec4d colour );

void SDL_InitVideo( SDLwindow_t &window );
void SDL_CreateContext( SDLwindow_t &window, SDL_GLContext context );
void SDL_SwapWindow( SDLwindow_t &window );
void SDL_Shutdown( SDLwindow_t &window, SDL_GLContext context );

void SDL_PrintOpenGLatributes();

} // namespace primer
#endif /* !__SIMPLEDIRECTMEDIALAYER_H__ */
