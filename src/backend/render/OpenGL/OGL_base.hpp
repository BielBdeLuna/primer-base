/*
this program is GPLv3
*/

#ifndef __OGL_BASE_H__
#define __OGL_BASE_H__

#include <GL/glew.h>
#include "../meshes.hpp"

namespace primer {

struct OpenGL_config_t {
  int version_major;
  int version_minor;
  int mask_profile;
  int doubleBuffered;
};

struct OpenGL_vertex_t {
  GLuint vboID[2];
  GLuint vaoID[1];
  const uint32_t positionAttributeIndex = 0, colorAttributeIndex = 1;
};

class blOpenGL {
public:
  bool	    SetUpBufferObjects( blMeshIntermediate mesh );

  void      Render_A();
  void	    Render_B();

  void	    CleanUpVertex();

  OpenGL_config_t   conf;
  OpenGL_vertex_t   OGLvertex;
};

} //namespace primer
#endif /* !__OGL_BASE_H__ */
