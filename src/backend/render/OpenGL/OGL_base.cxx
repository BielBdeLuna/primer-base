/*
this program is GPLv3
*/

#include "OGL_base.hpp"
#include "../../framework/common.hpp"

namespace primer {

bool blOpenGL::SetUpBufferObjects( blMeshIntermediate mesh ) {
  common->Printf( "Seting up the VBOs and VAO...\n" );

  //define a diamond shape
  const uint32_t vertexNum = mesh.GetVertexNum();
  const uint32_t floatsPerPoint = 3;
  const uint32_t floatsPerColour = 4;

  GLfloat diamond[vertexNum][floatsPerPoint];
  GLfloat colours[vertexNum][floatsPerColour];

  for( int i = 0; i < vertexNum; i++ ) {
    blVertex_t p = mesh.GetVertex( i );
    for( int j = 0; j <= ( floatsPerColour - 1 ); j++ ) {
      if( j <= ( floatsPerPoint - 1 ) ) {
        diamond[i][j] = p.position.GetData( j );
      }
      colours[i][j] = p.colour.GetData( j );
    }
  }
  /*
  common->Printf( "checking my stored data...\n" );

  for( int i = 0; i < vertexNum; i++ ) {
    common->Printf( "position at[%i] : ", i );
    for( int j = 0; j < floatsPerPoint; j++ ) {
      common->Printf( "data[%i]: %ff ", j, diamond[i][j] );
    }
    common->Printf( "\n");
  }

  for( int i = 0; i < vertexNum; i++ ) {
    common->Printf( "colour at[%i] : ", i );
    for( int j = 0; j < floatsPerColour; j++ ) {
      common->Printf( "data[%i]: %ff ", j, colours[i][j] );
    }
    common->Printf( "\n");
  }
  */

  //generate the buffers as well as the arrays
	glGenBuffers(2, OGLvertex.vboID);
	glGenVertexArrays(1, OGLvertex.vaoID);
	glBindVertexArray(OGLvertex.vaoID[0]);

	// Positions
  glBindBuffer( GL_ARRAY_BUFFER, OGLvertex.vboID[0] );
  glBufferData( GL_ARRAY_BUFFER, ( vertexNum * floatsPerPoint) * sizeof(GLfloat), diamond, GL_STATIC_DRAW );
  glVertexAttribPointer( OGLvertex.positionAttributeIndex, floatsPerPoint, GL_FLOAT, GL_FALSE, 0, 0 );
  glEnableVertexAttribArray( OGLvertex.positionAttributeIndex );

  // Colors
  glBindBuffer( GL_ARRAY_BUFFER, OGLvertex.vboID[1] );
  glBufferData( GL_ARRAY_BUFFER, ( vertexNum * floatsPerColour) * sizeof(GLfloat), colours, GL_STATIC_DRAW );
  glVertexAttribPointer( OGLvertex.colorAttributeIndex, 4, GL_FLOAT, GL_FALSE, 0, 0 );
  glEnableVertexAttribArray( OGLvertex.colorAttributeIndex );


  glBindBuffer(GL_ARRAY_BUFFER, 0); //NOTE: moved before the shader

  return true;
  /*
	// Set up shader ( will be covered in the next part )
	// ===================
	if ( !shaders.Init() )
		return false;

	shaders.UseProgram();

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	return true;
	*/
}


void blOpenGL::Render_A() {
  // First, render a square without any colors ( all vertexes will be black )
	// ===================
	// Make our background grey
	glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	// Invoke glDrawArrays telling that our data is a line loop and we want to draw 2-4 vertexes
	glDrawArrays(GL_LINE_LOOP, 0, 4);

}

void blOpenGL::Render_B() {
  // Second, enable the colors and draw a solid square
	// ===================
	// Enable our attribute within the current VAO
	glEnableVertexAttribArray( OGLvertex.colorAttributeIndex );

	// Make our background black
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	// Invoke glDrawArrays telling that our data is a line loop and we want to draw 2-4 vertexes
	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}

void blOpenGL::CleanUpVertex() {
  /*
  // Cleanup all the things we bound and allocated
	shaders.CleanUp();
	*/

	glDisableVertexAttribArray( 0 );
	glDeleteBuffers( 1, OGLvertex.vboID );
	glDeleteVertexArrays( 1, OGLvertex.vaoID );
}

} // namespace primer
