/*
this program is GPLv3
*/

#pragma once

#include <GL/glew.h>
#include "../../../sys/sys_public.hpp"

namespace primer {

class GLSLshaders {
public:
  bool Init();
  void BindAttributeLocation( int index, const std::string &attribute );
  void UseProgram();
  bool LoadVertexShader( const std::string &filename );
  bool LoadFragmentShader( const std::string &filename );
  bool LinkShaders();
  void PrintShaderLinkingError( int32_t shaderId );
  // If something went wrong whil compiling the shaders, we'll use this function to find the error
  void PrintShaderCompilationErrorInfo( int32_t shaderId );
  void CleanUp();

  GLuint shaderProgram;
  GLuint vertexshader, fragmentShader;
};

} //namespace primer
