/*
this program is GPLv3
*/

#include "shaders.hpp"
#include "../../framework/common.hpp"

namespace primer {

bool GLSLshaders::Init() {
  // Generate our shader. This is similar to glGenBuffers() and glGenVertexArray(), except that this returns the ID
  shaderProgram = glCreateProgram();

	// Bind the location of our attributes
	//BindAttributeLocation( 0, "in_Position" );
	//BindAttributeLocation( 1, "in_Color" );
	glBindAttribLocation( shaderProgram, 0, "in_Position" );
	glBindAttribLocation( shaderProgram, 1, "in_Color" );

	if( !LoadVertexShader( "basic_vertex.glsl" ) )
	  return false;

	if( !LoadFragmentShader( "basic_fragment.glsl" ) )
	  return false;

	// All shaders has been create, now we must put them together into one large object
	return LinkShaders();
}

void GLSLshaders::BindAttributeLocation(int index, const std::string &attribute) {
  // Bind attribute index 0 (coordinates) to in_Position and attribute index 1 (color) to in_Color
	// Attribute locations must be setup before calling glLinkProgram
	glBindAttribLocation( shaderProgram, index, attribute.c_str() );
}

void GLSLshaders::UseProgram() {
  // Load the shader into the rendering pipeline
  glUseProgram( shaderProgram );
}

bool GLSLshaders::LoadVertexShader( const std::string &filename ) {
  common->Printf( "Linking Vertex shader\n" );

  // Read file as std::string
	std::string str = Sys_ReadFile( filename.c_str() );

	// c_str() gives us a const char*, but we need a non-const one
	char* src = const_cast<char*>( str.c_str() );
	int32_t size = str.length();

	// Create an empty vertex shader handle
	vertexshader = glCreateShader( GL_VERTEX_SHADER );

	// Send the vertex shader source code to OpenGL
	glShaderSource( vertexshader, 1, &src, &size );

	// Compile the vertex shader
	glCompileShader( vertexshader );

	int wasCompiled = 0;
	glGetShaderiv( vertexshader, GL_COMPILE_STATUS, &wasCompiled );

	if( wasCompiled == 0 ) {
		PrintShaderCompilationErrorInfo( vertexshader );
		return false;
	}

	glAttachShader( shaderProgram, vertexshader );
	return true;
}

bool GLSLshaders::LoadFragmentShader( const std::string &filename ) {
  common->Printf( "Linking Fragment shader\n" );

  // Read file as std::string
	std::string str = Sys_ReadFile( filename.c_str() );

  // c_str() gives us a const char*, but we need a non-const one
	char* src = const_cast<char*>( str.c_str() );
	int32_t size = str.length();

	// Create an empty fragment shader handle
	fragmentShader = glCreateShader( GL_FRAGMENT_SHADER );

	// Send the fragment shader source code to OpenGL
	glShaderSource( fragmentShader, 1, &src, &size );

	// Compile the fragment shader
	glCompileShader( fragmentShader );

	int wasCompiled = 0;
	glGetShaderiv( fragmentShader, GL_COMPILE_STATUS, &wasCompiled );

	if( wasCompiled == false ) {
	  PrintShaderCompilationErrorInfo( fragmentShader );
		return false;
	}

	glAttachShader( shaderProgram, fragmentShader );
	return true;
}

bool GLSLshaders::LinkShaders() {
  // Link. At this point, our shaders will be inspected/optized and the binary code generated
	// The binary code will then be uploaded to the GPU
	glLinkProgram( shaderProgram );

	// Verify that the linking succeeded
	int isLinked;
	glGetProgramiv( shaderProgram, GL_LINK_STATUS, (int *)&isLinked );

	if( isLinked == false )
	  PrintShaderLinkingError( shaderProgram );

	return isLinked != 0;
}

void GLSLshaders::PrintShaderLinkingError( int32_t shaderId ) {
	common->Printf( "=======================================\nShader linking failed : \n" );

	// Find length of shader info log
	int maxLength;
	glGetProgramiv( shaderId, GL_INFO_LOG_LENGTH, &maxLength );

	common->Printf( "Info Length : %i\n", maxLength );

  // Get shader info log
	char* shaderProgramInfoLog = new char[maxLength];
	glGetProgramInfoLog( shaderProgram, maxLength, &maxLength, shaderProgramInfoLog );

	common->Printf( "Linker error message : %s\n", shaderProgramInfoLog );

	/* Handle the error in an appropriate way such as displaying a message or writing to a log file. */
	/* In this simple program, we'll just leave */
	delete shaderProgramInfoLog;
	return;
}

// If something went wrong whil compiling the shaders, we'll use this function to find the error
void GLSLshaders::PrintShaderCompilationErrorInfo( int32_t shaderId ) {
  common->Printf( "=======================================\nShader compilation failed : \n" );

  // Find length of shader info log
	int maxLength;
	glGetShaderiv( shaderId, GL_INFO_LOG_LENGTH, &maxLength );

	// Get shader info log
	char* shaderInfoLog = new char[maxLength];
	glGetShaderInfoLog( shaderId, maxLength, &maxLength, shaderInfoLog );

	// Print shader info log
	common->Printf( "\tError info : %s\n=======================================\n\n", shaderInfoLog );
	delete shaderInfoLog;
}

void GLSLshaders::CleanUp() {
  /* Cleanup all the things we bound and allocated */
	glUseProgram( 0 );
	glDetachShader( shaderProgram, vertexshader );
	glDetachShader( shaderProgram, fragmentShader );

	glDeleteProgram( shaderProgram );


	glDeleteShader( vertexshader );
	glDeleteShader( fragmentShader );
}

} //namespace primer


