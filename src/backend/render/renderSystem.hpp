/*
this program is GPLv3
*/

#ifndef __RENDERSYSTEM_H__
#define __RENDERSYSTEM_H__

#include "../sdl/SDL_base.hpp"

#include "../../frontend/content/content.hpp"

namespace primer {

class blRenderSystem {
public:
  virtual ~blRenderSystem() {}
  virtual bool Init() = 0;
  virtual void Shutdown() = 0;
  virtual void ClearColor( float r, float g, float b ) = 0;
  virtual void SwapWindow() = 0;

  virtual void SetContent( blContent the_content ) = 0;

  virtual bool Renderer_Init() = 0;
  virtual void Renderer_Render_A() = 0;
  virtual void Renderer_Render_B() = 0;
};

extern blRenderSystem*	renderSystem;

} // namespace primer
#endif /* !__RENDERSYSTEM_H__ */
