/*
this program is GPLv3
*/

#include "renderSystem.hpp"
#include "OpenGL/OGL_base.hpp"
#include "GLSL/shaders.hpp"

namespace primer {

class blRenderSystemLocal : public blRenderSystem {

public:
  blRenderSystemLocal();
  bool    Init() override;
  void    ClearColor( float r, float g, float b ) override;
  void    SwapWindow() override;
  void    Shutdown() override;

  void    SetContent( blContent the_content ) override;

  bool    Renderer_Init() override;
  void    Renderer_Render_A() override;
  void    Renderer_Render_B() override;

private:
  void    Clear();

  SDLwindow_t		window;
  SDL_GLContext	context;

  blOpenGL		Renderer;
  blContent		content;

  GLSLshaders	    	shaders;
};

extern blRenderSystemLocal renderSystemLocal;

} // namespace primer
