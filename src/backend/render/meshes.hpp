/*
this program is GPLv3
*/

#ifndef __MESHES_H__
#define __MESHES_H__

#include "../lib/maths/vectors.hpp"

namespace primer {

struct blVertex_t {
  Vec3d   position;
  Vec4d   colour;
};

class blVertexShape {
public:
  blVertex_t	GetVertex( int index );
  void 	AddVertex( blVertex_t v );
protected:
  std::vector<blVertex_t> data;
};

class blTriangle : public blVertexShape {
public:
	blTriangle( blVertex_t a, blVertex_t b, blVertex_t c );
};

class blQuad : public blVertexShape {
public:
	blQuad( blTriangle firstTriangle, blTriangle secondTriangle );
};

class blMeshIntermediate {
public:
  blMeshIntermediate() {};

  blVertex_t  GetVertex( int index );
  int	    GetVertexNum() const;
  void	    AddVertex( blVertex_t v );
  void      AddTriangle( blTriangle t );
  void      AddQuad( blQuad q );

private:
  bool      AlreadyHave( blVertex_t v );
  std::vector<blVertex_t> data;
};

} //namespace primer
#endif /* !__MESHES_H__ */
