/*
this program is GPLv3
*/

#include "meshes.hpp"
#include "../framework/common.hpp"

namespace primer {

blVertex_t blVertexShape::GetVertex( int index ) {
  return data[index];
}
void blVertexShape::AddVertex( blVertex_t v ) {
  int capacity = data.capacity();

  if( data.size() == capacity ) {
    data.reserve( capacity + 1 );
  }

  data.push_back( v );
}

blTriangle::blTriangle( blVertex_t a, blVertex_t b, blVertex_t c ) {
  data.reserve( 3 );
  data.push_back( a );
  data.push_back( b );
  data.push_back( c );
}

blQuad::blQuad( blTriangle firstTriangle, blTriangle secondTriangle ){
  // the reason for this is quite simple:
  // in a quad always two vertex are repeated in the contained triangles

  int i;

  data.reserve( 4 );

  // first let's add all the firstTriangle's vertices to the list straight out;
  for ( i = 0; i <= 2; i++ ) {
     data.push_back( firstTriangle.GetVertex( i ) );
  }

  // then let's compare the secondTriangle's vertices to the ones stored in the list,
  // at the first unmatch add it to the list and break the search
  for ( i = 0; i <= 2; i++ ) {
    //compare them by their positions
    if( !GetVertex( i ).position.Compare( secondTriangle.GetVertex( i ).position ) ) {
      data.push_back( secondTriangle.GetVertex( i ) );
      break;
    }
  }
}


// MESH STUFF
/*
blMeshIntermediate::blMeshIntermediate() {
  //common->Printf( "data started!\n" );
  //data.reserve( 0 );
  ;
}
*/

blVertex_t blMeshIntermediate::GetVertex( int index ) {
  blVertex_t v = data[index];
  return v;
}

int blMeshIntermediate::GetVertexNum() const {
  int size = data.size();
  return size;
}

bool blMeshIntermediate::AlreadyHave( blVertex_t v ) {
  int size = GetVertexNum();
  if( size != 0 ) {
    for( int i = 0; i <= size; i++ ) {
      //compare them by their positions
      if( data[i].position.Compare( v.position ) ) {
        //we already have this vertex
        return true;
      }
    }
  }
  return false;
}

void blMeshIntermediate::AddVertex( blVertex_t v ) {

  //only add it if we don't have it already
  if( !AlreadyHave( v ) ) {
    //only reserve new capacity in "data" if we are in the limit
    int size = GetVertexNum();
    int capacity = data.capacity();
    if( size == capacity ) {
      data.reserve( capacity + 1 );
    }
    data.push_back( v );
  }
}

void blMeshIntermediate::AddTriangle( blTriangle t ) {
  for( int i = 0; i <= 2; i++ ) {
    AddVertex( t.GetVertex( i ) );
  }
}

void blMeshIntermediate::AddQuad( blQuad q ) {
  for( int i = 0; i <= 3; i++ ) {
    AddVertex( q.GetVertex( i ) );
  }
}

} // namespace primer
