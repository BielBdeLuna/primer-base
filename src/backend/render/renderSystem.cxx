/*
this program is GPLv3
*/

#include "renderSystemLocal.hpp"
#include "../framework/common.hpp"
//#include "meshes.hpp"

namespace primer {

blRenderSystemLocal	renderSystemLocal;
blRenderSystem* 		renderSystem = &renderSystemLocal;

blRenderSystemLocal::blRenderSystemLocal() {
  Clear();

  Renderer.conf.version_major = 3;
  Renderer.conf.version_minor = 3;
  Renderer.conf.mask_profile = SDL_GL_CONTEXT_PROFILE_CORE; //TODO this is not an OGL term but SDL
  Renderer.conf.doubleBuffered = 1;

  window.title = "my OpenGL test";
  window.size_x = 512;
  window.size_y = window.size_x;
  window.pos_x = SDL_WINDOWPOS_CENTERED;
  window.pos_y = window.pos_x;
  window.window = NULL;

}

void blRenderSystemLocal::Clear() {
  /*
  if( content != NULL ) {
    delete content;
  }
  content = NULL;
  */
  ;
}

void blRenderSystemLocal::Shutdown() {

  // Cleanup all the things we bound and allocated
	shaders.CleanUp();

  //clear VBO and VAO as well as shaders
  Renderer.CleanUpVertex();

  //shutdown SDL2 gracefully
  SDL_Shutdown( window, context );

  Clear();
  common->Printf( " -- Render System shut down --\n" );
}

bool blRenderSystemLocal::Init() {
  common->Printf( " -- Render System init --\n" );

  //init SDL video
  SDL_InitVideo( window );

  SDL_CreateContext( window, context );

  SDL_SetOpenGLatributes( Renderer.conf );

  SDL_PrintOpenGLatributes();

  // This makes our buffer swap syncronized with the monitor's vertical refresh
	SDL_GL_SetSwapInterval(1);

  // Init GLEW
	glewExperimental = GL_TRUE;
	glewInit();

  Vec4d colour_gray;
  colour_gray.Set( 0.5f, 0.5f, 0.5f, 1.0f );

  SDL_ClearBuffer( window, colour_gray ); //this should be in the first frame before the loop starts

  if( !Renderer_Init() ) {
    return false;
  }

  if ( !shaders.Init() ) {
		return false;
  }

	shaders.UseProgram();

	common->Printf( " -- Render System initiated --\n");
	return true;
}

void blRenderSystemLocal::ClearColor( float r, float g, float b ) {
  glClearColor( r, g, b, 1.0 );
	glClear( GL_COLOR_BUFFER_BIT );
}

void blRenderSystemLocal::SwapWindow() {
  SDL_SwapWindow( window );
}

void blRenderSystemLocal::SetContent( blContent the_content ) {
  content = the_content;
}

bool blRenderSystemLocal::Renderer_Init() {
  if( !Renderer.SetUpBufferObjects( content.GetMesh() ) ) {
    common->Error( "blRenderSystemLocal::Renderer_Init() : couldn't load the buffer objects!\n" );
  }
  return true;
}

void blRenderSystemLocal::Renderer_Render_A() {
  common->Printf( "    Render A\n" );
  Renderer.Render_A();
  SwapWindow();
}

void blRenderSystemLocal::Renderer_Render_B() {
  common->Printf( "    Render B\n" );
  Renderer.Render_B();
  SwapWindow();
}

} // namespace primer
