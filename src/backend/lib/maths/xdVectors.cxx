/*
this program is GPLv3
*/

#include "vectors.hpp"
#include "../../framework/common.hpp"

namespace primer {


//////////////
//2D VECTORS//
//////////////

Vec2d::Vec2d() {
  SetDimensions( 2 );
  data.push_back( 0.0f );
  data.push_back( 0.0f );

}

Vec2d::Vec2d( const float xy ) {
  SetDimensions( 2 );
  data.push_back( xy );
  data.push_back( xy );
}

Vec2d::Vec2d( const float x, const float y ) {
  SetDimensions( 2 );
  data.push_back( x );
  data.push_back( y );
}

void Vec2d::Set( const float _x, const float _y ) {
  SetData( 0, _x );
  SetData( 1, _y );
}


//////////////
//3D VECTORS//
//////////////

Vec3d::Vec3d() {
  SetDimensions( 3 );
  data.push_back( 0.0f );
  data.push_back( 0.0f );
  data.push_back( 0.0f );

}

Vec3d::Vec3d( const float xyz ) {
  SetDimensions( 3 );
  data.push_back( xyz );
  data.push_back( xyz );
  data.push_back( xyz );
}

Vec3d::Vec3d( const float x, const float y, const float z ) {
  SetDimensions( 3 );
  data.push_back( x );
  data.push_back( y );
  data.push_back( z );
}

void Vec3d::Set( const float _x, const float _y, const float _z ) {
  SetData( 0, _x );
  SetData( 1, _y );
  SetData( 2, _z );
}


//////////////
//4D VECTORS//
//////////////

Vec4d::Vec4d() {
  SetDimensions( 4 );
  data.push_back( 0.0f );
  data.push_back( 0.0f );
  data.push_back( 0.0f );
  data.push_back( 0.0f );

}

Vec4d::Vec4d( const float xyzw ) {
  SetDimensions( 4 );
  data.push_back( xyzw );
  data.push_back( xyzw );
  data.push_back( xyzw );
  data.push_back( xyzw );
}

Vec4d::Vec4d( const float x, const float y, const float z, const float w ) {
  SetDimensions( 4 );
  data.push_back( x );
  data.push_back( y );
  data.push_back( z );
  data.push_back( w );
}

void Vec4d::Set( const float _x, const float _y, const float _z, const float _w ) {
  SetData( 0, _x );
  SetData( 1, _y );
  SetData( 2, _z );
  SetData( 3, _w );
}

float Vec4d::x() const {
  return GetData( 0 );
};

float Vec4d::y() const {
  return GetData( 1 );
};

float Vec4d::z() const {
  return GetData( 2 );
};

float Vec4d::w() const {
  return GetData( 3 );
};

} // namespace primer
