/*
this program is GPLv3
*/

#ifndef __VECTORS_HPP__
#define __VECTORS_HPP__

#include "../../../sys/sys_public.hpp"
#include <vector>

#include "maths.hpp"

namespace primer {

class blVector {
public:
  void ZeroOut();
  std::size_t GetDimensions() const;
  void SetDimensions( std::size_t num );
  float GetData( int index ) const;
  void SetData( int index, float value );
  bool Compare( const blVector &other ) const;
  float Length() const;
  float LengthSqr() const;
  float LengthFast() const;
  float Normalize() const; //TODO
  blVector Truncate( float len ); //TODO


  float  operator[]( int index ) const;
  bool	 operator==( const blVector &other ) const;
  bool	 operator!=( const blVector &other ) const;
  blVector operator-() const;
  blVector operator*( const float n ) const;
  blVector operator/( const float n ) const;
  blVector operator*( const blVector &other ) const;
  blVector operator+( const blVector &other ) const;
  blVector operator-( const blVector &other ) const;
  blVector& operator+=( const blVector &other );
  blVector& operator-=( const blVector &other );
  blVector& operator/=( const blVector &other );
  blVector& operator/=( const float n );
  blVector& operator*=( const float n );

protected:
  std::vector<float> data;
};

class Vec2d : public blVector {
public:
  Vec2d();
  explicit Vec2d( const float xy );
  explicit Vec2d( const float x, const float y );
  void Set( const float _x, const float _y );
  float x;/* { this->data[0]; }; */
  float y;/* { this->data[1]; }; */
};

class Vec3d : public blVector {
public:
  Vec3d();
  explicit Vec3d( const float xyz );
  explicit Vec3d( const float x, const float y, const float z );
  void Set( const float _x, const float _y, const float _z );
  //float x;/* { return GetData( 0 ); }; */
  //float y;/* { return GetData( 1 ); }; */
  //float z;/* { return GetData( 2 ); }; */
};

class Vec4d : public blVector {
public:
  Vec4d();
  explicit Vec4d( const float xyzw );
  explicit Vec4d( const float x, const float y, const float z, const float w );
  void Set( const float _x, const float _y, const float _z, const float _w );
  float x() const;/* { return GetData( 0 ); }; */
  float y() const;/* { return GetData( 1 ); }; */
  float z() const;/* { return GetData( 2 ); }; */
  float w() const;/* { return GetData( 3 ); }; */
};

/*
class Vec4d {
public:
  Vec4d();
  Vec4d( const float x, const float y, const float z, const float w );
  void Set( const float x, const float y, const float z, const float w );
  void ZeroOut();
  std::size_t GetDimensions() const;
  float GetInDimension( int index ) const;
  bool Compare( const blVector &other ) const;
  float x { data[0] };
  float y { data[1] };
  float z { data[2] };
  float w { data[3] };

  float  operator[]( int index ) const;
  bool	 operator==( const blVector &other ) const;
  bool	 operator!=( const blVector &other ) const;
  blVector operator-() const;
  blVector operator*( const float n ) const;
  blVector operator/( const float n ) const;
  blVector operator*( const blVector &other ) const;
  blVector operator+( const blVector &other ) const;
  blVector operator-( const blVector &other ) const;

private:
  std::vector<float> data;
};
*/
} // namespace primer
#endif /* !__VECTORS_HPP__ */
