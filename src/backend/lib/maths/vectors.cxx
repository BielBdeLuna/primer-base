/*
this program is GPLv3
*/

#include "vectors.hpp"
#include "../../framework/common.hpp"


namespace primer {

void blVector::ZeroOut() {
  int dimensions = data.size();
  if( dimensions ) {
    for( int i = 0; i < dimensions; i++ ) {
      data[i] = 0.0f;
    }
  }
}

std::size_t blVector::GetDimensions() const {
  return data.size();
}

void blVector::SetDimensions( std::size_t num ) {
  data.reserve( num );
}

float blVector::GetData( int index ) const {
  return data[index];
}

void blVector::SetData( int index, float value ) {
  if( data.capacity() <= index ) {
    common->Error( "blVector::SetData attempting to put value '%f' in index '%i' while the Vector has the only capacity of '%i'\n", value, index, data.capacity() );
  } else {
    data[index] = value;
  }
}

bool blVector::Compare( const blVector &other ) const {
  bool ret = false;
  if( GetDimensions() == other.GetDimensions() ) {
    ret = true;
    for( int i = 0; i < GetDimensions(); i++ ) {
      if( other.GetData( i ) != GetData( i ) ) {
        return false;
      }
    }
  }

  return ret;
}

float blVector::LengthSqr() const {
  std::size_t dimensions = this->GetDimensions();
  float sqrLength;
  for( int i = 0; i < dimensions; i++ ) {
    sqrLength += this->data[i] * this->data[i];
  }

  return sqrLength;
}

float blVector::Length() const {
  float ret = primer::blSqrt( LengthSqr() );
  return ret;
}

float blVector::LengthFast() const {
  float sqrLength = LengthSqr();
  return sqrLength * primer::blInvSqrt( sqrLength );
}

float blVector::operator[]( int index ) const {
	return data[index];
}

bool blVector::operator==( const blVector &other )  const {
	return Compare( other );
}

bool blVector::operator!=( const blVector &other )  const {
	return !Compare( other );
}

blVector blVector::operator-() const {
	blVector ret;
	std::size_t dimensions = this->GetDimensions();
	ret.SetDimensions( dimensions );

	for( int i = 0; i < dimensions; i++ ) {
	  float value = this->data[i] * -1;
	  ret.SetData( i, value );
	}

	return ret;
}

blVector blVector::operator*( const float n ) const {
	blVector ret;
	std::size_t dimensions = this->GetDimensions();
	ret.SetDimensions( dimensions );

	for( int i = 0; i < dimensions; i++ ) {
	  float value = this->data[i] * n;
	  ret.SetData( i, value );
	}

	return ret;
}

blVector blVector::operator/( const float n ) const {
	blVector ret;
	std::size_t dimensions = this->GetDimensions();
	ret.SetDimensions( dimensions );

	float p = ( 1.0f / n );

	for( int i = 0; i < dimensions; i++ ) {
	  float value = this->data[i] * p;
	  ret.SetData( i, value );
	}

	return ret;
}

blVector blVector::operator*( const blVector &other ) const {
	std::size_t dimensions = this->GetDimensions();
	if( dimensions != other.GetDimensions() ) {
	  common->Error( "blVector::operator* : triying to multiply two vectors with different dimensions!\n" );
	}

	blVector ret;
	ret.SetDimensions( dimensions );

	for( int i = 0; i < dimensions; i++ ) {
	  float value = this->data[i] * other.GetData( i );
	  ret.SetData( i, value );
	}

	return ret;
}

blVector blVector::operator+( const blVector &other ) const {
	std::size_t dimensions = this->GetDimensions();
	if( dimensions != other.GetDimensions() ) {
	  common->Error( "blVector::operator+ : triying to sum two vectors with different dimensions!\n" );
	}

	blVector ret;
	ret.SetDimensions( dimensions );

	for( int i = 0; i < dimensions; i++ ) {
	  float value = this->data[i] + other.GetData( i );
	  ret.SetData( i, value );
	}

	return ret;
}

blVector blVector::operator-( const blVector &other ) const {
	std::size_t dimensions = this->GetDimensions();
	if( dimensions != other.GetDimensions() ) {
	  common->Error( "blVector::operator- : triying to substract two vectors with different dimensions!\n" );
	}

	blVector ret;
	ret.SetDimensions( dimensions );

	for( int i = 0; i < dimensions; i++ ) {
	  float value = this->data[i] - other.GetData( i );
	  ret.SetData( i, value );
	}

	return ret;
}

blVector& blVector::operator+=( const blVector &other ) {
  std::size_t dimensions = this->GetDimensions();
  if( dimensions != other.GetDimensions() ) {
    common->Error( "blVector::operator+= : triying to sum two vectors with different dimensions!\n" );
  }

  for( int i = 0; i < dimensions; i++ ) {
    this->data[i] += other.GetData( i );
  }

  return *this;
}

blVector& blVector::operator-=( const blVector &other ) {
  std::size_t dimensions = this->GetDimensions();
  if( dimensions != other.GetDimensions() ) {
    common->Error( "blVector::operator+= : triying to substract two vectors with different dimensions!\n" );
  }

  for( int i = 0; i < dimensions; i++ ) {
    this->data[i] -= other.GetData( i );
  }

  return *this;
}

blVector& blVector::operator/=( const blVector &other ) {
  std::size_t dimensions = this->GetDimensions();
  if( dimensions != other.GetDimensions() ) {
    common->Error( "blVector::operator+= : triying to divide two vectors with different dimensions!\n" );
  }

  for( int i = 0; i < dimensions; i++ ) {
    this->data[i] /= other.GetData( i );
  }

  return *this;
}

blVector& blVector::operator/=( const float n ) {
  float p = ( 1.0f / n );
  for( int i = 0; i < this->GetDimensions(); i++ ) {
    this->data[i] *= p;
  }

  return *this;
}

blVector& blVector::operator*=( const float n ) {
  for( int i = 0; i < this->GetDimensions(); i++ ) {
    this->data[i] *= n;
  }

  return *this;
}

} // namespace primer
