/*
this program is GPLv3
*/

#ifndef __MATHS_HPP__
#define __MATHS_HPP__

#include "../../../sys/sys_public.hpp"
#include <cmath>

namespace primer {

float blSqrt( const float n );
float blInvSqrt( const float n );

} // namespace primer
#endif /* !__MATHS_HPP__ */
