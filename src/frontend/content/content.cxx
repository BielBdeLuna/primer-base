/*
this program is GPLv3
*/

#include "content.hpp"
#include "../../backend/lib/maths/vectors.hpp"
#include "../../backend/framework/common.hpp"

namespace primer {

void blContent::Init() {
  Vec3d pos;
  Vec4d col;
  blVertex_t  vert;
  pos.Set( -0.5f,  0.5f,  0.5f );
  vert.position = pos;
  col.Set( 0.0f, 1.0f, 0.0f, 1.0f );
  vert.colour = col;
  mesh.AddVertex( vert );

  pos.Set( 0.5f,  0.5f,  0.5f );
  vert.position = pos;
  col.Set( 1.0f, 1.0f, 0.0f, 1.0f );
  vert.colour = col;
  mesh.AddVertex( vert );

  pos.Set( 0.5f, -0.5f,  0.5f );
  vert.position = pos;
  col.Set( 1.0f, 0.0f, 0.0f, 1.0f );
  vert.colour = col;
  mesh.AddVertex( vert );

  pos.Set( -0.5f,  -0.5f,  0.5f );
  vert.position = pos;
  col.Set( 0.0f, 0.0f, 1.0f, 1.0f );
  vert.colour = col;
  mesh.AddVertex( vert );
  /*
  common->Printf( "checking out my generated data...\n\n" );
  for( int i = 0; i < mesh.GetVertexNum(); i++ ) {
    blVertex_t  vert = mesh.GetVertex( i );
    float posX = vert.position.GetData( 0 );
    float posY = vert.position.GetData( 1 );
    float posZ = vert.position.GetData( 2 );
    float colR = vert.colour.GetData( 0 );
    float colG = vert.colour.GetData( 1 );
    float colB = vert.colour.GetData( 2 );
    float colW = vert.colour.GetData( 3 );
    common->Printf( "vertex [%i]\nPOSITION: ' %f, %f, %f '\nCOLOUR: ' %f, %f, %f, %f '\n",
                    i, posX, posY, posZ, colR, colG, colB, colW );
  }
  */
}

blMeshIntermediate blContent::GetMesh() {
  return mesh;
}

} //namespace primer
