/*
this program is GPLv3
*/

#ifndef __CONTENT_H__
#define __CONTENT_H__

#include "../../sys/sys_public.hpp"
#include "../../backend/render/meshes.hpp"

namespace primer {

class blContent {
public:
  void Init();
  blMeshIntermediate GetMesh();

private:
  blMeshIntermediate mesh;
};

} // namespace primer
#endif /* !__CONTENT_H__ */

