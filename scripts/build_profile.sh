#!/bin/bash

SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $SCRIPTS_DIR #get into the script's dir, and operate from there

cd ..
rm -r ./build/
mkdir ./build/
cd ./build/
BUILD_DIR=$(pwd -P)

cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release ../

if [ $? -eq 0 ]; then
    echo
    echo
    echo -e "    \e[5m-\e[0m \e[1;32m\e[5mDONE\e[0m \e[5m-\e[0m    now in \e[1;36m$BUILD_DIR/\e[0m directory"
    echo
    exec bash
else
    echo
    echo
    echo -e "    \e[5m-\e[0m \e[1;31m\e[5mFAIL\e[0m \e[5m-\e[0m"
    echo
fi
